const VOCAB_INDEX: &str = "data/index.txt";

use std::process::exit;
use std::fs;
use std::io;
use std::io::BufRead;
use rand::Rng;

fn readf(fp: &String) -> Vec<String>{
    let file = fs::File::open(fp)
        .expect("No file of that name exists.");
    let br = io::BufReader::new(&file);
    let lines = br.lines();
    let mut files: Vec<String> = vec![];
    for line in lines{
        let line = line.expect("Failed to extract line.");
        files.push(line);
    }
    files
}

fn choose_file() -> usize{
    let lessons_fp = readf(&VOCAB_INDEX.to_string());
    for (i, lesson_fp) in lessons_fp.iter().enumerate(){
        println!("{}: {}", i, lesson_fp);
    }
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Read input failed.");
    if input == "gg" {
        exit(0);
    }
    let input: usize = input.trim().parse()
        .expect("Enter a number.");
    input
}

fn fill_lists(fp: &usize) -> (Vec<String>, Vec<String>, Vec<usize>){
    let questions = readf(&format!("data/{}/question.txt", fp));
    let answers = readf(&format!("data/{}/answer.txt", fp));
    let banlist = vec![0; questions.len()];
    (questions, answers, banlist)
}

fn get_qnum(banlist: &Vec<usize>, time: &usize) -> usize{
    let mut bannum = 0;
    for bantime in banlist.iter(){
        if bantime > time{
            bannum += 1;
        }
    }
    let mut qnum:usize = rand::thread_rng().gen_range(0, banlist.len()-bannum);
    for (i, bantime) in banlist.iter().enumerate(){
        if time < bantime && qnum >= i{
            qnum += 1;
        }
    }
    qnum
}

fn ask(q: &String, a: &String) -> bool{
    println!("{}", q);
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Read input failed.");
    if input.trim() == "gg"{
        exit(0);
    }
    *a == input.trim()
}

fn main_loop(questions: &Vec<String>, answers: &Vec<String>, banlist: &mut Vec<usize>){
    let mut correctnum = 0;
    let mut total = 0usize;
    loop{
        let qnum = get_qnum(&banlist, &total);
        total += 1;
        let correct = ask(&questions[qnum], &answers[qnum]);
        if correct{
            correctnum += 1;
            banlist[qnum] = total + banlist.len() / 3;
            println!("Correct");
        }
        else{
            banlist[qnum] = total + 2;
            println!("Incorrect");
            println!("correct answer: {}", answers[qnum]);
        }
        println!("score: {}/{}", correctnum, total);
        println!();
    }
}

fn main() {
    let lesson_fp = choose_file();
    let (questions, answers, mut banlist) = fill_lists(&lesson_fp);
    print!("{}[2J", 27 as char);
    main_loop(&questions, &answers, &mut banlist);
}
